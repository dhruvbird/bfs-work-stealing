#if !defined GETRUSAGE_H
#define GETRUSAGE_H

#include <sys/time.h>
#include <sys/resource.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include <string>
using namespace std;

void
print_rusage() {
    struct rusage usage, usage1;
    int r = getrusage(RUSAGE_SELF, &usage);
    r = getrusage(RUSAGE_CHILDREN, &usage1);
    usage.ru_nvcsw += usage1.ru_nvcsw;
    usage.ru_nivcsw += usage1.ru_nivcsw;
    pid_t pid = getpid();
    char filename[512];
    sprintf(filename, "/proc/%d/status", pid);
    FILE *pf = fopen(filename, "r");
    char status[4096];
    fread(status, 1, 4095, pf);
    status[4095] = '\0';
    string sstatus = status;
    size_t pos = sstatus.find("VmRSS:");

    char mem[15], units[8];
    mem[0] = units[0] = '\0';

    if (pos != std::string::npos) {
        pos += strlen("VmRSS:");
        sscanf(status + pos, "%s %s", mem, units);
    }

    printf("Max_RSS: %s %s\n"
           "Voluntary_Context_Switches: %lu\n"
           "Involuntary_Context_Switches: %lu\n",
           mem, units, usage.ru_nvcsw, usage.ru_nivcsw);
}

#endif // GETRUSAGE_H
