CC=cilk++
CFLAGS=-O3

all: 
	$(CC) $(CFLAGS) -o sbfs sBFS_1July.cilk -lmiser
	$(CC) $(CFLAGS) -o pbfs_cwl pBFS1_centralized_withlock.cilk -lmiser
	$(CC) $(CFLAGS) -o pbfs_clf pBFS1_centralized_lockfree.cilk -lmiser
	$(CC) $(CFLAGS) -o pbfsws_sflfo pBFSws_lockfree_scalefree_cacheopt.cilk -lmiser
	$(CC) $(CFLAGS) -o pbfsws_sfl pBFSws_lock_scalefree.cilk -lmiser
	$(CC) $(CFLAGS) -o pbfsws_wl pBFSws_withlock.cilk -lmiser
	$(CC) $(CFLAGS) -o pbfsws_sflf pBFSws_lockfree_scalefree.cilk -lmiser
	$(CC) $(CFLAGS) -o pbfsws_scwl pBFSws_withlock_scalefree.cilk -lmiser
	$(CC) $(CFLAGS) -o pbfsws_lf pBFSws_lockfree.cilk -lmiser
	$(CC) $(CFLAGS) -o inputSorter inputSorter.cpp 
	cilk++ -O3 -o pbfsd pBFS1_dcentralized_lockfree.cilk
	cilk++ -O3 -o pbfscd pBFS1_centralizedDistributed_lockfree.cilk

clean:
	rm -f sbfs pbfs_cwl pbfs_clf pbfsws_sflfo pbfsws_wl pbfsws_sflf pbfsws_scwl pbfsws_lf inputSorter
	rm -f output-hw*
	rm -f error-hw*
