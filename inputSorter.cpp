#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
#include <utility>
using namespace std;

typedef pair<int,int> pii;

#define MAX_SOURCES 1000
int main() {
  int n, m, s;
  vector<int> sources;
  sources.reserve(n);

  int ret = scanf("%d%d%d", &n, &m, &s);
  assert(ret != EOF && ret != 0);

  pii* pa = new pii[m];

  for (int i = 0; i < m; i++) {
    int u, v;
    int ret = scanf("%d%d", &u, &v);
    assert(ret != EOF && ret != 0);
    pa[i] = make_pair(u,v);
  }
  sort(pa, pa+m);
  vector<int> source_list;
  
  for (int i = 0; i < m; i++) {
    if ((source_list.empty() || source_list.back() != pa[i].first) && (pa[i].first != pa[i].second)) {
          source_list.push_back(pa[i].first);
    }
  }
  
  s = min(MAX_SOURCES, (int)source_list.size());
  random_shuffle(source_list.begin(), source_list.end());

  /*
  int* sa = new int[s];
  for (int i = 0; i < s; i++) {
    scanf("%d", &sa[i]);
  }*/

  printf("%d %d %d\n", n, m, s);
  fprintf(stderr, "%d\n", s);
  for (int i = 0; i < m; i++) {
    printf("%d %d\n", pa[i].first, pa[i].second);
  }

  for (int i = 0; i < s; i++) {
    printf("%d\n", source_list[i]);
    fprintf(stderr, "%d\n", source_list[i]);
 }
}
