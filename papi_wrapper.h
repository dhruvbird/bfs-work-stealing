#if !defined PAPI_WRAPPER_H
#define PAPI_WRAPPER_H

#include <assert.h>
#include <stdlib.h>
#if defined HAVE_PAPI
#include <papi.h>
#endif

// #define HAVE_PAPI

namespace {
#if defined HAVE_PAPI
    int EventSet = PAPI_NULL;
    int retval;
    char errstring[PAPI_MAX_STR_LEN];
    long long values[NUM_EVENTS];
#endif
}

void papi_initialize() {
#if defined HAVE_PAPI
    if((retval = PAPI_library_init(PAPI_VER_CURRENT)) != PAPI_VER_CURRENT ) {
        fprintf(stderr, "Error: %s\n", errstring);
        exit(1);
    }

    /* Creating event set   */
    if ((retval=PAPI_create_eventset(&EventSet)) != PAPI_OK) {
        fprintf("Error %d in PAPI_create_eventset\n", retval);
        abort();
    }

    /* Add the array of events PAPI_TOT_INS and PAPI_TOT_CYC to the eventset*/
    if ((retval=PAPI_add_event(EventSet, PAPI_L1_DCM)) != PAPI_OK) {
        fprintf("Error %d in PAPI_add_event\n", retval);
        abort();
    }

    if ( PAPI_add_event( EventSet, PAPI_L2_DCM ) != PAPI_OK) {
        fprintf("Error %d in PAPI_add_event[2]\n", retval);
        abort();
    }
#endif
}

void papi_start() {
#if defined HAVE_PAPI
    /* Start counting */
    if ( (retval=PAPI_start(EventSet)) != PAPI_OK) {
        fprintf("Error %d in PAPI_start[2]\n", retval);
        abort();
    }
#endif
}


void papi_stop() {
#if defined HAVE_PAPI
    if ( (retval=PAPI_stop(EventSet,values)) != PAPI_OK) {
        fprintf("Error %d in PAPI_stop[2]\n", retval);
        abort();
    }
#endif
}

void papi_report() {
#if defined HAVE_PAPI
    std::cout << "L1 Misses: " << values[0] << " L2 Misses: " << values[1] << std::endl;
#endif
}

void papi_destroy() {
#if defined HAVE_PAPI
    if ( (retval=PAPI_remove_event(EventSet,PAPI_L1_DCM))!=PAPI_OK) {
        fprintf("Error %d in PAPI_remove_event[1]\n", retval);
        abort();
    }

       
    if ( (retval=PAPI_remove_event(EventSet,PAPI_L2_DCM))!=PAPI_OK) {
        fprintf("Error %d in PAPI_remove_event[2]\n", retval);
        abort();
    }

    /* Free all memory and data structures, EventSet must be empty. */
    if ( (retval=PAPI_destroy_eventset(&EventSet)) != PAPI_OK) {
        fprintf("Error %d in PAPI_destroy_eventset\n", retval);
        abort();
    }

    /* free the resources used by PAPI */
    PAPI_shutdown();
#endif
}

#endif
