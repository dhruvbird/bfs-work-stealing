CILKFLAGS="$CILKFLAGS -O2 -Wall -lmiser"
ls *.cilk | while read file
do
    echo "Compiling: '$file'"
    outf=${file//\.cilk/}
    cilk++ $CILKFLAGS $file -o $outf
done
